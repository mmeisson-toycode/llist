#ifndef SLIST_H
# define SLIST_H

# include <assert.h>
# include <stdlib.h>
# include <string.h>

struct s_list;

typedef struct s_list s_list;
typedef struct s_list s_node;

typedef struct      s_list
{
    struct s_node *next;
}                   s_list;

/*
 *  s_list_iter gives a safe way to iterate over a list
 *
 *  It can be used even while removing items being used
 */
typedef struct      s_list_iter
{
    struct s_node   *list;
    struct s_node   *current;
    struct s_node   *next;
}                   s_list_iter;


/*
 *  Initialize an empty list
 *
 * \return an empty list pointer
 */
static inline s_list        *list_new(void)
{
    return NULL;
}

/*
 *  Initialize an empty node
 *  The node is entirelly set to 0 and node->next is NULL
 *
 *  \return the heap allocated node or NULL
 */
static inline s_node        node_init(void)
{
    return (s_node *){ .next = NULL };
}

/*
 *  Append a node at the end of a list
 *  this function has an O(n) complexity
 *
 * \param head of a list or intermediary node
 */
static inline void          list_append(s_list **head, s_node *node)
{
    s_node      *iter = (s_node *)*head;

    if (*head == NULL)
        *head = (s_list *)node;
    else
    {
        while (iter->next != NULL)
            iter = iter->next;
        iter->next = node;
    }
}

/*
 *  Append a node at the end of a list
 *  this function has an O(1) complexity
 *
 * \param head of a list or intermediary node
 */
static inline void          list_prepend(s_list **head, s_node *node)
{
    node->next = (s_node *)*head;
    *head = (s_list *)node;
}

/*
 * Creates an iterator from a list
 */
static inline s_list_iter   list_iter_init(s_list *list)
{
    return (s_list_iter) {
        .list = list,
        .current = list,
        .next = (list != NULL) ? list->next : NULL,
    };
}

/*
 * Set the iterator's internal node to the next one
 *
 * \return node pointed after the iteration
 */
static inline s_node        *list_iter_next(s_list_iter *iterator)
{
    assert(iterator != NULL);

    iterator->current = iterator->next;
    iterator->next = (iterator->current != NULL) ? iterator->current->next : NULL;
    return iterator->current;
}

/*
 *  Returns pointer to the node currently pointer by the iterator
 *
 *  Does not mutate iterator's state
 */
static inline s_node        *list_iterator_current(const s_list_iter *iterator)
{
    return iterator->current;
}

/*
 * Returns 0 if the iterator has no more item pointed out,
 * 1 instead
 */
static inline int           list_iter_ended(s_list_iter *iterator)
{
    return iterator->current == NULL;
}


/*
 *  A simple loop all over a list
 *
 *  example :
 *    for_list(list, node) {
 *      printf("current node == %p\n", node);
 *    }
 */
#define for_list(list, node_name) for ( \
            s_node *node_name = list; \
            node_name != NULL; \
            node_name = node_name->next \
        )

/*
 *  A loop all over a list with a safe iterator
 *
 *  example :
 *    for_list_iter(list, node) {
 *      printf("current node == %p\n", node);
 *    }
 */
#define for_list_iter(list, node_name) for ( \
            s_list_iter _iterator = list_iter_init(), \
                s_node *node_name = list_iter_current(_iterator); \
            list_iter_ended(&_iterator) != 0; \
            list_iter_next(&iterator) \
        )


static s_node               *_list_reverse(s_list *node)
{
    s_node  *next;
    s_node  *prev;

    assert(node != NULL && node->next != NULL);
    prev = (s_node *)node;
    node = node->next;
    prev->next = NULL;

    while (node->next != NULL)
    {
        next = node->next;
        node->next = prev;
        prev = node;
        node = next;
    }
    node->next = prev;
    return node;
}

/*
 * Reverse a list in place
 * With an O(n) complexity
 */
static inline s_list		*list_reverse(s_list **head)
{
    if (*head != NULL
        && ((s_node *)*head)->next != NULL)
        {
            *head = (s_list *)_list_reverse(*head);
        }
    }
	return *head;
}

/*
 *  Apply a callback on each node of a list
 *
 *  \return a new list made of callback's returns
 *  \hint It can be safelly used as a filter, each NULL is ignored. But remember to
 *  free unused ressources in the callback
 */
static inline s_list        *list_map(s_list *head, s_node *(*callback)(s_node *))
{
    s_list      *res = list_new();
    s_node      *new_node;

    for_list_iter(head, current_node)
    {
        new_node = callback(current_node);

        if (new_node != NULL)
        {
            list_prepend(&list, new_node);
        }
    }
    list_reverse(&list);
    return list;
}

#endif
